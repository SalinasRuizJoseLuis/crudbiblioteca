<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Listado de Libros</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
  <?php include("../conecta.php"); ?>
</head>
<?php


  $query = 'select id_autor, nombre_autor from biblioteca.autor';

  $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());
?>

<body>
<table>
  <caption>Listado de Autores</caption>
  <thead>
    <tr>
      <th>#</th>
      <th>ID</th>
      <th>Autor</th>
      <th>Opción</th>
    </tr>
  </thead>
  <tbody>

<?php
  $contador = 1;
  while ($tupla = pg_fetch_array($resultado, null, PGSQL_ASSOC)) {
    $id = $tupla['id_autor'];
?>
    <tr>
      <td>
        <?php echo $contador++; ?>
      </td>
<?php
    foreach ($tupla as $atributo) {
?>
      <td><?php echo trim($atributo); ?></td>
<?php
    }
?>
      <td>
        <a href="confirmar-autor.php?id=<?php echo $id; ?>">Editar Información</a>
      </td>
    </tr>
<?php
  }

  pg_free_result($result);
  pg_close($dbconn);
?>

  </tbody>
</table>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
</ul>

</body>
</html>
