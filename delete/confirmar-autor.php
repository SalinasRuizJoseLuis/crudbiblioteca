<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Información de Autores</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $id = $_GET['id'];
  $error = false;
  if (empty($id)) {
    $error = true;
?>
  <p>Error, no se ha indicado el ID del autor</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "hola";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select id_autor, nombre_autor
      from biblioteca.autor
      where id_autor = '".$id."';";

    $libro = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($libro) == 0) {
      $error = true;
?>
  <p>No se han encontrado Autores con ese ID <?php echo $isbn; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($libro, null, PGSQL_ASSOC);
      $nom = $tupla['nombre_autor'];
?>
<table>
  <caption>Información del Autor</caption>
  <tbody>
    <tr>
      <th>ID</th>
      <td><?php echo $id; ?></td>
    </tr>
    <tr>
      <th>Nombre</th>
      <td><?php echo $nom; ?></td>
    </tr>
    <tr>
      <th>Titulo/es</th>
      <td>
<?php
      $query = "select titulo_libro
        from biblioteca.autor as a
        inner join biblioteca.libro_autor as la  on a.id_autor=la.id_autor
			inner join biblioteca.libro as l on l.isbn=la.isbn
			 where a.id_autor=('".$id."');";
			
			
      

      $autores = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($autores) == 0) {
?>
        <p>Sin autor</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($autores, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
          <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
?>
    </tr>
    <tr>
      <th>ISBN/es</th>
      <td>
<?php
      $query = "select isbn
        from biblioteca.libro_autor
        where id_autor = '".$id."';";

      $ejemplares = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($ejemplares) == 0) {
?>
        <p>Sin ejemplares</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($ejemplares, null, PGSQL_ASSOC)) {
          $isbn = $tupla['isbn'];
        
?>
          <li><?php echo $isbn." / "; ?></li> 
<?php
        }
?>
        </ul>
<?php
      }
    }
  }
?>
    </tr>
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);

  if (!$error) {
?>
<form action="delete-autores.php" method="post">
  <input type="hidden" name="id" value="<?php echo $id; ?>" />
  <p>¿Está seguro/a de eliminar este Autor?</p>
  <input type="submit" name="submit" value="DELETE" />
  <p>
    Se borrarán a su vez todos las relaciones de este autor.
  </p>
</form>

<form action="autor.php" method="post">
  <input type="submit" name="submit" value="Cancelar" />
</form>
<?php
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="autor.php">Lista de Autores</a></li>
</ul>

</body>
</html>
