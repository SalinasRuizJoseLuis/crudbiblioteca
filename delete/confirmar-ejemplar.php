<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Información de Ejemplares</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $isbn = $_GET['isbn'];
  $error = false;
  if (empty($isbn)) {
    $error = true;
?>
  <p>Error, no se ha indicado el ISBN del Ejemplar</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "hola";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select clave_ejemplar,conservacion_ejemplar,isbn
      from biblioteca.ejemplar
      where isbn = '".$isbn."';";

    $libro = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($libro) == 0) {
      $error = true;
?>
  <p>No se ha encontrado algún Ejemplar con ISBN <?php echo $isbn; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($libro, null, PGSQL_ASSOC);
      $titulo_libro = $tupla['titulo_libro'];
?>
<table>
  <caption>Información de libro</caption>
  <tbody>
    <tr>
      <th>ISBN</th>
      <td><?php echo $isbn; ?></td>
    </tr>
   
    <tr>
      <th>Clave</th>
      <td>
<?php
      $query = "select clave_ejemplar
      from biblioteca.ejemplar
      where isbn = '".$isbn."';";
      $autores = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($autores) == 0) {
?>
        <p>Sin autor</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($autores, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
          <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
?>
    </tr>
    <tr>
      <th>Ejemplar/es</th>
      <td>
<?php
      $query = "select clave_ejemplar, conservacion_ejemplar
        from biblioteca.ejemplar
        where isbn = '".$isbn."';";

      $ejemplares = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($ejemplares) == 0) {
?>
        <p>Sin ejemplares</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($ejemplares, null, PGSQL_ASSOC)) {
          $clave_ejemplar = $tupla['clave_ejemplar'];
          $conservacion_ejemplar = $tupla['conservacion_ejemplar'];
?>
          <li><?php echo $clave_ejemplar." / ".$conservacion_ejemplar; ?></li> 
<?php
        }
?>
        </ul>
<?php
      }
    }
  }
?>
    </tr>
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);

  if (!$error) {
?>
<form action="delete-ejemplar.php" method="post">
  <input type="hidden" name="isbn" value="<?php echo $isbn; ?>" />
  <p>¿Está seguro/a de eliminar este libro?</p>
  <input type="submit" name="submit" value="DELETE" />
  <p>
    Se borrarán a su vez todos los ejemplares en existencia
  </p>
</form>

<form action="ejemplares.php" method="post">
  <input type="submit" name="submit" value="Cancelar" />
</form>
<?php
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplares.php">Lista de Ejemplares</a></li>
</ul>

</body>
</html>
