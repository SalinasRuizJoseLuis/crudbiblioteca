<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de Autores</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<?php
  $id = $_POST['id'];
  if (empty($id)) {
?>
  <p>Error, no se indico el ID del Autor</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "hola";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select id_autor,nombre_autor
      from biblioteca.autor
      where id_autor = '".$id."';";

    $libro = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($libro) == 0) {
?>
  <p>No se ha encontrado algún Autor con ese ID <?php echo $id; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($libro, null, PGSQL_ASSOC);
      $id_au = $tupla['nombre_autor'];

      $query = "delete from biblioteca.autor where id_autor = '".$id."';";
      $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      
    
      
      if (pg_affected_rows($resultado) == 0) {
?>
  <p>Error al momento de borrar al Autor</p>
<?php
      } else {
?>
  <p>El Autor con id <?php echo $id; ?> y Nombre "<?php echo $id_au; ?>" fue borrado con exito.</p>
<?php
      }
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="autor.php">Lista de Autores</a></li>
</ul>

</body>
</html>
