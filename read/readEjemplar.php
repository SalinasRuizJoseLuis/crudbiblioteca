<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Información de libro</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
  <?php include("../conecta.php"); ?>
</head>
<body>
<?php
  $clave = $_GET['clave_ejemplar'];

  if (empty($clave)) {
?>
  <p>Error, no se ha indicado la clave del libro</p>
<?php
  } else {
    

    $query = "select clave_ejemplar,conservacion_ejemplar,e.isbn,a.id_autor,nombre_autor
 from biblioteca.ejemplar as e inner join biblioteca.libro_autor as la on e.isbn=la.isbn
inner join biblioteca.autor as a on la.id_autor=a.id_autor
      where e.clave_ejemplar = '".$clave."';";

    $libro = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($libro) == 0) {
?>
  <p>No se ha encontrado algún libro con la clave <?php echo $clave; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($libro, null, PGSQL_ASSOC);
      $titulo_libro = $tupla['Clave'];
?>
<table>
  <caption>Información de Ejemplar</caption>
  <tbody>
    <tr>
      <th>CLAVE</th>
      <td><?php echo $clave; ?></td>
    </tr>
    <tr>
      <th>ISBN</th>
      <td>
		  
		  <?php
        $query = "select distinct(e.isbn)
 from biblioteca.ejemplar as e inner join biblioteca.libro_autor as la on e.isbn=la.isbn
inner join biblioteca.autor as a on la.id_autor=a.id_autor
      where e.clave_ejemplar = '".$clave."';";

      $isbn = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($isbn) == 0) {
?>
        <p>Sin autor</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($isbn, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
          <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
?>

		  
		  
    </tr>
    
    
    
    <tr>
      <th>Conservacion</th>
      <td>
<?php
        $query = "select distinct(conservacion_ejemplar)
 from biblioteca.ejemplar as e inner join biblioteca.libro_autor as la on e.isbn=la.isbn
inner join biblioteca.autor as a on la.id_autor=a.id_autor
      where e.clave_ejemplar = '".$clave."';";

      $conserva = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($conserva) == 0) {
?>
        <p>Sin autor</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($conserva, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
          <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
?>
    </tr>
    
        </tr>
        
        
        
    <tr>
      <th>Autores</th>
      <td>
<?php
        $query = "select distinct(nombre_autor)
 from biblioteca.ejemplar as e inner join biblioteca.libro_autor as la on e.isbn=la.isbn
inner join biblioteca.autor as a on la.id_autor=a.id_autor
      where e.clave_ejemplar = '".$clave."';";

      $autores = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($autores) == 0) {
?>
        <p>Sin autor</p>
<?php
      } else {
?>
        <ul>
<?php
        while ($tupla = pg_fetch_array($autores, null, PGSQL_ASSOC)) {
          foreach ($tupla as $atributo) {
?>
          <li><?php echo $atributo; ?></li> 
<?php
          }
        }
?>
        </ul>
<?php
      }
?>
    </tr>
    
    
    
    <tr>
      <th></th>
      <td>
<?php
     

      $ejemplares = pg_query($query) or die('La consulta falló: ' . pg_last_error());
      if (pg_num_rows($ejemplares) == 0) {
?>
        <p>Sin ejemplares</p>
<?php
      } else {
?>
        
<?php
      }
    }
  }
?>
    </tr>
  </tbody>
</table>

<?php
  pg_free_result($result);
  pg_close($dbconn);
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="libros.php">Lista de libros</a></li>
</ul>

</body>
</html>
