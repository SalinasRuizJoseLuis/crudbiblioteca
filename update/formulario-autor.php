<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de libro</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
   <?php include("../conecta.php"); ?>
</head>
<body>
<?php
  $id = $_GET['id'];

  if (empty($id)) {
?>
  <p>Error, no se ha indicado el ID del autor</p>
<?php
  } else {
    
    $query = "select id_autor, nombre_autor
      from biblioteca.autor
      where id_autor = '".$id."';";

    $libro = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($libro) == 0) {
?>
  <p>No se ha encontrado algún Autor con ese ID <?php echo $isbn; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($libro, null, PGSQL_ASSOC);
      $autor = $tupla['nombre_autor'];
?>
<form action="update-autor.php" method="post">
<table>
  <caption>Información de libro</caption>
  <tbody>
    <tr>
      <th>ID</th>
      <td><input type="text" name="id_autor" value="<?php echo $id; ?>" /></td>
    </tr>
    <tr>
      <th>Nombre Autor:</th>
      <td><textarea name="nombre_autor"><?php echo $autor; ?></textarea></td>
    </tr>
  </tbody>
</table>
<input type="submit" name="submit" value="UPDATE" />
</form>
<?php
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="autores.php">Lista de libros</a></li>
</ul>

</body>
</html>
