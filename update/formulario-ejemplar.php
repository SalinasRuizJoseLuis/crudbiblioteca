<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de libro</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $isbn = $_GET['isbn'];

  if (empty($isbn)) {
?>
  <p>Error, no se ha indicado el ISBN del libro</p>
<?php
  } else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "hola";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select isbn,conservacion_ejemplar
      from biblioteca.ejemplar
      where isbn = '".$isbn."';";

    $libro = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($libro) == 0) {
?>
  <p>No se ha encontrado algún ejemplar con ISBN <?php echo $isbn; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($libro, null, PGSQL_ASSOC);
      $conservacion= $tupla['conservacion_ejemplar'];
?>
<form action="update-ejemplar.php" method="post">
<table>
  <caption>Información de Ejempalr</caption>
  <tbody>
    <tr>
      <th>ISBN</th>
      <td><input type="text" name="isbn" value="<?php echo $isbn; ?>" /></td>
    </tr>
    <tr>
      <th>Conservacion</th>
      <td><textarea name="conserva"><?php echo $conservacion; ?></textarea></td>
    </tr>
  </tbody>
</table>
<input type="submit" name="submit" value="UPDATE" />
</form>
<?php
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplares.php">Lista de Ejemplares</a></li>
</ul>

</body>
</html>
