<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario de Ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<?php
  $error = false;
  $isbn = $_POST['isbn'];
  $conservacion = $_POST['conserva'];

  if (empty($isbn)) {
    $error = true;
?>
  <p>Error, no se indico el ISBN del ejemplar</p>
<?php
  }
  if (empty($conservacion)) {
    $error = true;
?>
  <p>Error, no se indico la condicion del ejemplar</p>
<?php
  }

  if (!$error) {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "hola";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select isbn
      from biblioteca.ejemplar
      where isbn = '".$isbn."';";

    $libro = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($libro) == 0) {
?>
  <p>No se ha encontrado algún Ejemplar con ISBN <?php echo $isbn; ?></p>
<?php
    } else {
      $query = "update biblioteca.ejemplar
        set conservacion_ejemplar= '".$conservacion."'
        where isbn = '".$isbn."';";

      $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());

      if (pg_affected_rows($resultado) == 0) {
?>
  <p>Error al momento de guardar los datos del Ejemplar</p>
<?php
      } else {
?>
  <p>Los datos del Ejemplar con ISBN <?php echo $isbn; ?> han sido actualizados con exito</p>
<?php
      }
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplares.php">Lista de libros</a></li>
</ul>

</body>
</html>
